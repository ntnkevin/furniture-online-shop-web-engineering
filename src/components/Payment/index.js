import React from "react";
import * as ROUTES from "../../constants/routes";
import { useParams, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import firebase from "firebase";
import "firebase/firestore";

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "0px",
    paddingTop: "50px",
    paddingBottom: "25px",
    width: "1000px",
    backgroundColor: "#fff",
  },
  content: {},
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
}));

const PaymentPage = () => {
  let { id } = useParams();
  const classes = useStyle();
  const history = useHistory();
  // const [dataOrder, setDataOrder] = React.useState([]);
  const [productList, setProductList] = React.useState([]);

  const fetchProduct = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("order")
      .doc(id)
      .get()
      .then(function (doc) {
        const item = {
          price: doc.data().price,
          owner: doc.data().owner,
        };
        result.push(item);
      });
    setProductList(result);
  };

  React.useEffect(() => {
    fetchProduct();
    //eslint-disable-next-line
  }, []);

  const payNow = () => {
    const db = firebase.firestore();
    db.collection("order").doc(id).update({ status: "PAID" });
    alert("Thank you for trusting us !");
    history.push(ROUTES.LIST_ORDER);
  };

  const payLater = () => {
    history.push(ROUTES.LIST_ORDER);
  };

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        <center>
          <h1>Payment Details</h1>
          {productList.map((item, key) => (
            <table key={key}>
              <tr>
                <td>
                  <h2>Your e-mail</h2>
                </td>
                <td>
                  <h2>:</h2>
                </td>
                <td>
                  <h2>{item.owner}</h2>
                </td>
              </tr>
              <tr>
                <td>
                  <h2>ID Purchase</h2>
                </td>
                <td>
                  <h2>:</h2>
                </td>
                <td>
                  <h2>{id}</h2>
                </td>
              </tr>
              <tr>
                <td>
                  <h2>Total payment</h2>
                </td>
                <td>
                  <h2>:</h2>
                </td>
                <td>
                  <h2>Rp. {numberWithCommas(item.price)},00</h2>
                </td>
              </tr>
            </table>
          ))}
          <br></br>
          <Button
            size="large"
            onClick={payLater}
            style={{
              backgroundColor: "black",
              color: "white",
              marginRight: "50px",
            }}
          >
            Pay Later!
          </Button>
          <Button
            size="large"
            onClick={payNow}
            style={{ backgroundColor: "black", color: "white" }}
          >
            Pay Now!
          </Button>
        </center>
      </div>
    </div>
  );
};

export default PaymentPage;
