import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";

const AddProduct = () => {
  const [txtName, setProductName] = React.useState(""); //Hooks
  const [txtCategory, setProductCategory] = React.useState(""); //Hooks
  const [txtDesc, setProdutDesc] = React.useState(""); //Hooks
  const [txtPrice, setProductPrice] = React.useState(""); //Hooks

  const addDataToFb = () => {
    const db = firebase.firestore();
    db.collection("Product")
      .add({
        Name: txtName,
        Category: txtCategory,
        Desc: txtDesc,
        Price: txtPrice,
      })
      .then(() => {
        alert("SUDAH DITAMBAHKAN!!!!!!");
      });
  };

  return (
    <div>
      <h2>Add Product To DB</h2>
      <label>Name Product : </label>
      <input
        type="text"
        value={txtName}
        onChange={(ev) => {
          setProductName(ev.target.value);
        }}
      />
      <br></br>
      <label>Category Product : </label>
      <input
        type="text"
        value={txtCategory}
        onChange={(ev) => {
          setProductCategory(ev.target.value);
        }}
      />
      <br></br>
      <label>Description : </label>
      <input
        type="text"
        value={txtDesc}
        onChange={(ev) => {
          setProdutDesc(ev.target.value);
        }}
      />
      <br></br>
      <label>Price : </label>
      <input
        type="text"
        value={txtPrice}
        onChange={(ev) => {
          setProductPrice(ev.target.value);
        }}
      />
      <br></br>
      <button onClick={addDataToFb}>ADD DATA</button>
    </div>
  );
};

const AddProductComp = () => {
  return (
    <div>
      <AddProduct />
    </div>
  );
};

export default AddProductComp;
