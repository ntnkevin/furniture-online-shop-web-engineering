import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import ProductHeroLayout from "./ProductLayout";
import Background from "../../image/background-1.jpg";

const styles = (theme) => ({
  background: {
    backgroundImage: `url(https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2FMonteCarlo-5S-Corner-Lounge-Suite.jpg?alt=media&token=35531e68-a4b4-460b-a484-944278e56160)`,
    backgroundColor: "#7fc7d9", // Average color of the background image.
    backgroundPosition: "center",
  },
  button: {
    minWidth: 200,
  },
  h5: {
    marginBottom: theme.spacing(4),
    marginTop: theme.spacing(4),
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(10),
    },
  },
  more: {
    marginTop: theme.spacing(2),
  },
});

function ProductHeader(props) {
  const { classes } = props;

  return (
    <ProductHeroLayout backgroundClassName={classes.background}>
      {/* Increase the network loading priority of the background image. */}
      <img
        style={{ display: "none" }}
        src={Background}
        alt="increase priority"
      />
      <Typography color="inherit" align="center" variant="h2" marked="center">
      Welcome to a world of inspiration
      </Typography>
      <Typography
        color="inherit"
        align="center"
        variant="h5"
        className={classes.h5}
      >
        Enjoy secret offers up to -70% off the best luxury furniture.
      </Typography>
      <Button
        color="secondary"
        variant="contained"
        size="large"
        className={classes.button}
        component="a"
        href="/signup/"
      >
        Register
      </Button>
      <Typography variant="body2" color="inherit" className={classes.more}>
        Discover the experience
      </Typography>
    </ProductHeroLayout>
  );
}

ProductHeader.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductHeader);
