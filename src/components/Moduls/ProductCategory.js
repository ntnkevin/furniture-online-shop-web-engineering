import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ButtonBase from "@material-ui/core/ButtonBase";
import Container from "@material-ui/core/Container";
import Typography from "../SubComponenet/Typograph";
import { useHistory } from "react-router-dom";

const styles = (theme) => ({
  root: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
  },
  images: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexWrap: "wrap",
  },
  imageWrapper: {
    position: "relative",
    display: "block",
    padding: 0,
    borderRadius: 0,
    height: "40vh",
    [theme.breakpoints.down("sm")]: {
      width: "100% !important",
      height: 100,
    },
    "&:hover": {
      zIndex: 1,
    },
    "&:hover $imageBackdrop": {
      opacity: 0.15,
    },
    "&:hover $imageMarked": {
      opacity: 0,
    },
    "&:hover $imageTitle": {
      border: "4px solid currentColor",
    },
  },
  imageButton: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: "cover",
    backgroundPosition: "center 40%",
  },
  imageBackdrop: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    background: theme.palette.common.black,
    opacity: 0.5,
    transition: theme.transitions.create("opacity"),
  },
  imageTitle: {
    position: "relative",
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px 14px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    background: theme.palette.common.white,
    position: "absolute",
    bottom: -2,
    left: "calc(50% - 9px)",
    transition: theme.transitions.create("opacity"),
  },
});

function ProductCategories(props) {
  const { classes } = props;

  const images = [
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2Famazon-rivet-furniture-1533048038.jpg?alt=media&token=183a8027-9bec-44be-a222-8954c095e108",
      title: "Sofa",
      width: "40%",
      path: "/product",
    },
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2Ffurniture-event-offer-kitchen.jpg?alt=media&token=b6f6b321-66a7-48a7-a860-bc01b373af1b",
      title: "Dinning Table",
      width: "20%",
      path: "/product",
    },
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2Fcharlotablunarova-com-M0FIbfxhK64-unsplash-255b2e6e7b524fe1aa895d817a6cb460.jpg?alt=media&token=bd9337e7-d6ab-43ce-bdba-c8e149e93f24",
      title: "Wood theme",
      width: "40%",
      path: "/product",
    },
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2FNorwegianBloggers_InTheVillage_LOWRES_008.jpg?alt=media&token=27e96a8d-f357-4edc-9659-c60ce214b4f9",
      title: "Dining ",
      width: "38%",
      path: "/product",
    },
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2Ftoscana-dining-table-tuscan-chestnut-c.jpg?alt=media&token=a198bba3-6a82-497d-a707-2927df1faa56",
      title: "Family Room",
      width: "38%",
      path: "/product",
    },
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2FDM_2048x.jpg?alt=media&token=af7d3e1a-473b-4e10-ac7d-2dd2aa5cf776",
      title: "Chair",
      width: "24%",
      path: "/product",
    },
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2F122919_m_super_furniture_storage.jpg?alt=media&token=1b095795-7ca5-4c8c-8eb4-e846acf50476",
      title: "Frame",
      width: "40%",
      path: "/product",
    },
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2F122919_m_super_furniture_dining.jpg?alt=media&token=07b0813d-daa8-4248-a63c-799369f96e44",
      title: "dining room",
      width: "20%",
      path: "/product",
    },
    {
      url:
        "https://firebasestorage.googleapis.com/v0/b/belajar-firebase-7a1be.appspot.com/o/Wallpaper%2F122919_m_super_furniture_bed.jpg?alt=media&token=a3a2924c-18f3-4275-a57d-15db30f8603c",
      title: "sleeping room",
      width: "40%",
      path: "/product",
    },
  ];

  const history = useHistory();
  const GoTo = (ParameterRoute) => {
    history.push(ParameterRoute);
  };

  return (
    <Container className={classes.root} component="section">
      <Typography variant="h4" marked="center" align="center" component="h2">
        Explore Our Inspiration
      </Typography>
      <div className={classes.images}>
        {images.map((image) => (
          <ButtonBase
            key={image.title}
            className={classes.imageWrapper}
            style={{
              width: image.width,
            }}
          >
            <div
              className={classes.imageSrc}
              style={{
                backgroundImage: `url(${image.url})`,
              }}
            />
            <div className={classes.imageBackdrop} />
            <div className={classes.imageButton}>
              <Typography
                component="h3"
                variant="h6"
                color="inherit"
                className={classes.imageTitle}
                onClick={() => {
                  GoTo(image.path);
                }}
              >
                {image.title}
                <div className={classes.imageMarked} />
              </Typography>
            </div>
          </ButtonBase>
        ))}
      </div>
    </Container>
  );
}

ProductCategories.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductCategories);
