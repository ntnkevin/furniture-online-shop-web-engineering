import React from "react";
import firebase from "firebase";
import "firebase/firestore";
import { useParams } from "react-router-dom";
import MainContex from "../MainContex";
import { makeStyles, Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import * as ROUTES from "../../constants/routes";

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "0px",
    paddingTop: "25px",
    width: "1000px",
    backgroundColor: "#fff",
  },
  content: {
    marginTop: "15px",
    paddingLeft: "70px",
    paddingBottom: "30px",
  },
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
  tr: {},
  trr: {
    paddingLeft: "30px",
    paddingRight: "30px",
    borderRadius: "10px",
  },
  divimg: {
    paddingTop: "100px",
    width: "250px",
    paddingRight: "50px",
    height: "300px",
    backgroundColor: "white",
    textAlign: "center",
    verticalAlign: "middle",
  },
}));

const OrderPage = () => {
  let { id } = useParams();
  const [txtQty, setTxtQty] = React.useState("");
  const [productList, setProductList] = React.useState([]);
  const classes = useStyle();
  const history = useHistory();
  const contex = React.useContext(MainContex);

  const fetchProduct = async () => {
    const db = firebase.firestore();
    const result = [];
    const productRef = db.collection("Product").doc(id);
    await productRef.get().then((doc) => {
      const item = {
        category: doc.data().Category,
        desc: doc.data().Desc,
        nama: doc.data().Name,
        price: doc.data().Price,
        photoUrl: doc.data().Url,
      };
      result.push(item);
    });
    setProductList(result);
  };

  React.useEffect(() => {
    fetchProduct();
    //eslint-disable-next-line
  }, []);

  const product = {
    idProduct: id,
    qty: txtQty,
  };

  const orderProduct = Array(product);
  var productPrice;

  const totalPrice = () => {
    var total;
    var intPrice = parseInt(productPrice, 10);
    total = intPrice * txtQty;

    return total;
  };

  const addOrderToDb = () => {
    var result = totalPrice();
    const db = firebase.firestore();
    db.collection("order")
      .add({
        owner: contex.email,
        item: orderProduct,
        status: "pending",
        price: result,
      })
      .then(function (docRef) {
        console.log("Document written with ID: ", docRef.id);
        history.push(`/payment/${docRef.id}`);
      });
  };

  const handlerSubmitLogin = () => {
    try {
      addOrderToDb();
      alert("Order Created");
    } catch (error) {
      console.log(error);
    }
  };

  const LoginUser = () => {
    return (
      <Button
        style={{
          marginLeft: "280px",
          backgroundColor: "rgba(27,27,27,0.8)",
          color: "white",
        }}
        onClick={handlerSubmitLogin}
      >
        Click To Confirm Order and Pay !
      </Button>
    );
  };

  const handlerSubmitNonLogin = () => {
    alert("Please Login first to buy our product!");
    history.push(ROUTES.SIGN_IN);
  };

  const NonLogin = () => {
    return (
      <Button
        style={{
          marginLeft: "280px",
          backgroundColor: "rgba(27,27,27,0.8)",
          color: "white",
        }}
        onClick={handlerSubmitNonLogin}
      >
        Click To Confirm Order and Pay !
      </Button>
    );
  };

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        <div className={classes.content}>
          <h1>Buy this Product</h1>
          {productList.map((item, key) => (
            <div key={key}>
              <table>
                <tr>
                  <td className={classes.tr} width="250px">
                    <div className={classes.divimg}>
                      <img
                        src={item.photoUrl}
                        width="150px"
                        height="100px"
                        alt="photoUrl"
                      ></img>
                    </div>
                  </td>
                  <td className={classes.trr} width="400px">
                    <h3>{item.nama}</h3>
                    <hr></hr>
                    <h3>
                      Rp. {numberWithCommas((productPrice = item.price))},00
                    </h3>
                    <hr></hr>
                    <label style={{ display: "inline" }}>QTY : </label>
                    <input
                      style={{ display: "inline" }}
                      type="text"
                      size="4"
                      value={txtQty}
                      onChange={(ev) => setTxtQty(ev.target.value)}
                    ></input>
                    <hr></hr>
                    <p style={{ textAlign: "justify" }}>{item.desc}</p>
                  </td>
                </tr>
              </table>
            </div>
          ))}
          {contex.email !== "" ? <LoginUser /> : <NonLogin />}
        </div>
      </div>
    </div>
  );
};

export default OrderPage;
