import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import * as ROUTES from "../../constants/routes";
import { useParams, useHistory } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";

const EditProduct = () => {
  let { id } = useParams();
  const history = useHistory();
  const [txtNameProduct, setTxtNameProduct] = React.useState("");
  const [txtDescProduct, setTxtDescProduct] = React.useState("");
  const [txtCategoryProduct, setTxtCategoryProduct] = React.useState("");
  const [txtPriceProduct, setTxtPriceProduct] = React.useState("");

  const fetchProduct = async () => {
    const db = firebase.firestore();
    const result = [];
    const productRef = db.collection("Product").doc(id);
    await productRef.get().then((doc) => {
      const item = {
        category: doc.data().Category,
        desc: doc.data().Desc,
        name: doc.data().Name,
        price: doc.data().Price,
      };
      setTxtNameProduct(doc.data().Name);
      setTxtDescProduct(doc.data().Desc);
      setTxtCategoryProduct(doc.data().Category);
      setTxtPriceProduct(doc.data().Price);

      result.push(item);
    });
  };

  React.useEffect(() => {
    fetchProduct();
    //eslint-disable-next-line
  }, []);

  const editData = () => {
    const db = firebase.firestore();
    db.collection("Product").doc(id).update({
      Category: txtCategoryProduct,
      Desc: txtDescProduct,
      Name: txtNameProduct,
      Price: txtPriceProduct,
    });
  };

  const editHandler = () => {
    editData();
    alert("Data Edited");
    history.push(ROUTES.PRODUCT);
  };

  return (
    <div style={{ marginTop: "80px", marginBottom: "100px" }}>
      <Container maxWidth="sm">
        <Typography variant="h6" gutterBottom>
          Edit Product id : {id}
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              id="productName"
              name="productName"
              label="Product Name"
              value={txtNameProduct}
              fullWidth
              onChange={(ev) => {
                setTxtNameProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="descName"
              name="descName"
              label="Description Name"
              value={txtDescProduct}
              fullWidth
              onChange={(ev) => {
                setTxtDescProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="cateProduct"
              name="cateProduct"
              label="Category Product"
              value={txtCategoryProduct}
              fullWidth
              onChange={(ev) => {
                setTxtCategoryProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="productProce"
              name="productPrice"
              label="Product Price"
              value={txtPriceProduct}
              fullWidth
              onChange={(ev) => {
                setTxtPriceProduct(ev.target.value);
              }}
            />
          </Grid>
          <Button
            style={{ backgroundColor: "rgba(27,27,27,0.8)", color: "#fff" }}
            onClick={() => {
              editHandler();
            }}
          >
            Edit
          </Button>
        </Grid>
      </Container>
    </div>
  );
};

export default EditProduct;
