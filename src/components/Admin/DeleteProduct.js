import React from "react";
import firebase from "firebase";
import "firebase/firestore";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";

const DeleteProduct = () => {
  const [idProduct, setIdProduct] = React.useState("");

  const deleteItem = (idProduct) => {
    const db = firebase.firestore();
    db.collection("Product").doc(idProduct).delete();
  };

  const handleSubmit = async () => {
    try {
      deleteItem(idProduct);
      alert(`Product Deleted`);
      // console.log("Data Added");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div style={{ marginTop: "50px", marginBottom: "100px" }}>
      <Container maxWidth="sm">
        <Typography variant="h6" gutterBottom>
          Delete Product
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              id="productId"
              name="productId"
              label="Product Id"
              value={idProduct}
              fullWidth
              onChange={(ev) => {
                setIdProduct(ev.target.value);
              }}
            />
          </Grid>
        </Grid>
        <br></br>
        <button onClick={handleSubmit}>Delete</button>
      </Container>
    </div>
  );
};

export default DeleteProduct;
