import React from "react";
import firebase from "firebase";
import "firebase/firestore";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
// if request.auth != null;

const AddProduct = () => {
  const [txtNameProduct, setTxtNameProduct] = React.useState("");
  const [txtDescProduct, setTxtDescProduct] = React.useState("");
  const [txtCategoryProduct, setTxtCategoryProduct] = React.useState("");
  const [txtPriceProduct, setTxtPriceProduct] = React.useState("");
  const [image, setImage] = React.useState(null);

  const updateDb = async (imgUrl) => {
    const db = firebase.firestore();
    await db.collection("Product").add({
      Category: txtCategoryProduct,
      Desc: txtDescProduct,
      Name: txtNameProduct,
      Price: txtPriceProduct,
      Url: imgUrl,
    });
  };

  const refreshPage = () => {
    window.location.reload(false);
  };

  const uploadImage = async () => {
    var storageRef = firebase.storage().ref(`Product/${image.name}`);
    await storageRef.put(image); //Proses Upload!
    const imgUrl = await storageRef.getDownloadURL();

    await updateDb(imgUrl);
  };

  const handleSubmit = async () => {
    try {
      await uploadImage();
      alert(`DATA ADDED`);
      refreshPage();
      // console.log("Data Added");
    } catch (error) {
      console.log(error);
    }
  };

  const handleFile = (event) => {
    if (event.target.files[0]) {
      const img = event.target.files[0];
      setImage(img);
    }
  };

  return (
    <div style={{ marginTop: "50px", marginBottom: "100px" }}>
      <Container maxWidth="sm">
        <Typography variant="h6" gutterBottom>
          Add Product
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              id="productName"
              name="productName"
              label="Product Name"
              value={txtNameProduct}
              fullWidth
              onChange={(ev) => {
                setTxtNameProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="descName"
              name="descName"
              label="Description Name"
              value={txtDescProduct}
              fullWidth
              onChange={(ev) => {
                setTxtDescProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="cateProduct"
              name="cateProduct"
              label="Category Product"
              value={txtCategoryProduct}
              fullWidth
              onChange={(ev) => {
                setTxtCategoryProduct(ev.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="productProce"
              name="productPrice"
              label="Product Price"
              value={txtPriceProduct}
              fullWidth
              onChange={(ev) => {
                setTxtPriceProduct(ev.target.value);
              }}
            />
          </Grid>
        </Grid>
        <br></br>
        <input
          type="file"
          onChange={handleFile}
          accept="image/*"
          style={{ paddingRight: "200px" }}
        />
        <button onClick={handleSubmit}>Add Product</button>
      </Container>
    </div>
  );
};

export default AddProduct;
