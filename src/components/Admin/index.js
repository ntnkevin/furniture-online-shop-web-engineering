import React from "react";
import MainContex from "../MainContex";
import { Redirect } from "react-router-dom";
import * as ROUTES from "../../constants/routes";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import AddProduct from "./AddProduct";
import DeleteProduct from "./DeleteProduct";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const NonAdmin = () => {
  return <Redirect to={ROUTES.LANDING}></Redirect>;
};

const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: "wrap",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[700],
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
}));

const AdminNavbar = () => {
  const classes = useStyles();
  return (
    <Router>
      <div>
        <AppBar
          position="static"
          color="default"
          elevation={0}
          className={classes.appBar}
        >
          <Toolbar className={classes.toolbar}>
            <Typography
              variant="h6"
              color="inherit"
              noWrap
              className={classes.toolbarTitle}
            >
              Admin Page's
            </Typography>
            <nav>
              <Link
                variant="button"
                color="textPrimary"
                className={classes.link}
                to={ROUTES.ADMIN_ADD_PRODUCT}
              >
                Add Product Tab
              </Link>
              <Link
                variant="button"
                color="textPrimary"
                className={classes.link}
                to={ROUTES.ADMIN_DELETE_PRODUCT}
              >
                Delete Product Tab
              </Link>
            </nav>
          </Toolbar>
        </AppBar>
        <Switch>
          <Route exact path={ROUTES.ADMIN_ADD_PRODUCT} component={AddProduct} />
          <Route
            exact
            path={ROUTES.ADMIN_DELETE_PRODUCT}
            component={DeleteProduct}
          />
        </Switch>
      </div>
    </Router>
  );
};

const Admin = () => {
  const contex = React.useContext(MainContex);
  // var user = firebase.auth().currentUser;
  return (
    <div style={{ marginTop: "70px" }}>
      {/* <h1>Admin Page</h1> */}
      {contex.email === "admin@gmail.com" ? <AdminNavbar /> : <NonAdmin />}
    </div>
  );
};

export default Admin;
