import React from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";

import Button from "@material-ui/core/Button";

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "0px",
    paddingTop: "50px",
    width: "1000px",
    height: "560px",
    backgroundColor: "#fff",
  },
  content: {
    marginTop: "50px",
    paddingLeft: "70px",
  },
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
}));

const DetailProduct = () => {
  let { id } = useParams();
  const [productList, setProductList] = React.useState([]);
  const history = useHistory();
  const classes = useStyle();

  const fetchProduct = async () => {
    const db = firebase.firestore();
    const result = [];
    const productRef = db.collection("Product").doc(id);
    await productRef.get().then((doc) => {
      const item = {
        category: doc.data().Category,
        desc: doc.data().Desc,
        nama: doc.data().Name,
        price: doc.data().Price,
        photoUrl: doc.data().Url,
      };
      result.push(item);
    });
    setProductList(result);
  };

  React.useEffect(() => {
    fetchProduct();
    //eslint-disable-next-line
  }, []);

  const handlerBuy = () => {
    history.push(`/product/buy/${id}`);
  };

  const handlerAddToChart = () => {
    return console.log("ADD TO CHART CLICKED");
  };

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        {productList.map((item, key) => (
          <div key={key}>
            <div className={classes.row}>
              <div className={classes.column}>
                <img
                  src={item.photoUrl}
                  width="400px"
                  height="300px"
                  alt="photoUrl"
                  style={{ paddingLeft: "50px" }}
                />
              </div>
            </div>

            <h1>{item.nama}</h1>
            <table style={{ marginRight: "50px" }}>
              <tr>
                <td>
                  <p>Price</p>
                </td>
                <td>:</td>
                <td>
                  <h1 style={{ color: "#fa591d", display: "inline" }}>
                    Rp.{numberWithCommas(item.price)},00
                  </h1>
                </td>
              </tr>
              <tr>
                <td>
                  <p>Category</p>
                </td>
                <td>:</td>
                <td>
                  <h3>{item.category}</h3>
                </td>
              </tr>
              <tr>
                <td>
                  <p>Description</p>
                </td>
                <td>:</td>
                <td>
                  <p style={{ textAlign: "justify" }}>{item.desc}</p>
                </td>
              </tr>
            </table>
            <br></br>
            <Button
              variant="contained"
              className={classes.button}
              component="a"
              style={{
                marginLeft: "500px",
                width: "450px",
                color: "white",
                backgroundColor: "rgb(27,27,27,0.8)",
              }}
              onClick={handlerAddToChart}
            >
              Add to Cart
            </Button>
            <br></br>
            <br></br>
            <Button
              variant="contained"
              className={classes.button}
              component="a"
              style={{
                marginLeft: "500px",
                width: "450px",
                color: "white",
                backgroundColor: "rgb(27,27,27,0.8)",
              }}
              onClick={handlerBuy}
            >
              Buy
            </Button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default DetailProduct;
