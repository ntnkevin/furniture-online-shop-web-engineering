import React from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import firebase from "firebase/app";
import "firebase/firestore";
import MainContex from "../MainContex";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  topTop: {
    marginTop: "50px",
  },
}));

const ProductPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const [menuList, setMenuList] = React.useState([]);
  const contex = React.useContext(MainContex);

  const fetchMenu = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("Product")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            id: doc.id,
            category: doc.data().Category,
            desc: doc.data().Desc,
            nama: doc.data().Name,
            price: doc.data().Price,
            photoUrl: doc.data().Url,
          };
          result.push(item);
        });
      });
    setMenuList(result);
  };

  React.useEffect(() => {
    fetchMenu();
  }, []);

  const deleteItem = (idProduct) => {
    const db = firebase.firestore();
    db.collection("Product").doc(idProduct).delete();
  };

  const refreshPage = () => {
    window.location.reload(false);
  };

  const AdminButton = (props) => {
    return (
      <div>
        <Button
          size="small"
          style={{
            backgroundColor: "rgba(27,27,27,0.8)",
            color: "white",
          }}
          onClick={() => {
            history.push(`/admin/edit-product/${props.idProduct}`);
          }}
        >
          Edit
        </Button>
        <Button
          size="small"
          style={{
            backgroundColor: "rgba(27,27,27,0.8)",
            color: "white",
            marginLeft: "20px",
          }}
          onClick={() => {
            deleteItem(props.idProduct);
            alert(`Product with id ${props.idProduct} has been deleted`);
            refreshPage();
          }}
        >
          Delete
        </Button>
      </div>
    );
  };

  const NonAdmin = (props) => {
    return (
      <div>
        <Button
          size="small"
          style={{
            backgroundColor: "rgba(27,27,27,0.8)",
            color: "white",
          }}
          onClick={() => {
            history.push(`/product/detail/${props.idProduct}`);
          }}
        >
          Detail
        </Button>

        <Button
          size="small"
          style={{
            backgroundColor: "rgba(27,27,27,0.8)",
            color: "white",
            marginLeft: "20px",
          }}
          onClick={() => {
            history.push(`/product/buy/${props.idProduct}`);
          }}
        >
          Buy
        </Button>
      </div>
    );
  };

  return (
    <div>
      <CssBaseline />
      <main>
        <div className={classes.topTop}>
          <Container className={classes.cardGrid} maxWidth="md">
            <h1>Our Products</h1>
            <Grid container spacing={4}>
              {menuList.map((item, key) => (
                <Grid item key={key} xs={12} sm={6} md={4}>
                  <Card className={classes.card}>
                    <CardMedia
                      className={classes.cardMedia}
                      image={item.photoUrl}
                      title={item.nama}
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant="h5" component="h2">
                        {item.nama}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      {contex.email === "admin@gmail.com" ? (
                        <AdminButton idProduct={item.id} />
                      ) : (
                        <NonAdmin idProduct={item.id} />
                      )}
                    </CardActions>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Container>
        </div>
      </main>
    </div>
  );
};

export default ProductPage;
