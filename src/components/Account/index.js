import React from "react";
import MainContex from "../MainContex";
import firebase from "firebase/app";
import "firebase/firestore";
import { makeStyles } from "@material-ui/core/styles";
// import MainContext from "../MainContex";

const AccountPage = () => {
  const contex = React.useContext(MainContex);
  const [accountList, setAccountList] = React.useState([]);

  const fetchAccount = async () => {
    const db = firebase.firestore();
    const result = [];
    var email = contex.email;
    // console.log(`Fetching account ${contex.email}`);
    // console.log("context" + contex.email);
    await db
      .collection("account")
      .where("email", "==", email)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            alamat: doc.data().alamat,
            name: doc.data().name,
            noTelepone: doc.data().noTelepon,
            avatar: doc.data().avatar,
          };
          result.push(item);
        });
      });
    setAccountList(result);
  };

  React.useEffect(() => {
    fetchAccount();
    //eslint-disable-next-line
  }, []);

  const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: "#f2f2f2",
      paddingBottom: "15px",
      paddingTop: "20px",
    },
    header: {
      textAlign: "center",
    },
    divuser: {
      marginTop: "60px",
      marginBottom: "0px",
      paddingTop: "30px",
      width: "450px",
      backgroundColor: "#fff",
      marginLeft: "auto",
      marginRight: "auto",
      paddingBottom: "30px",
    },
    content: {
      marginTop: "50px",
      paddingLeft: "70px",
    },
  }));

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.divuser}>
        {accountList.map((item, key) => (
          <div key={key}>
            <h1 className={classes.header}> {item.name}'s Profile</h1>
            <center>
              <img
                src={item.avatar}
                width="200px"
                height="200px"
                alt="avatar"
              ></img>
            </center>
            <div className={classes.content}>
              <p>Email</p>
              <h3>{contex.email}</h3>
              <p>Alamat</p>
              <h3>{item.alamat}</h3>
              <p>Phone Number</p>
              <h3>{item.noTelepone}</h3>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default AccountPage;
