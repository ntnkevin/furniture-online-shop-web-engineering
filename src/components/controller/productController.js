import firebase from "firebase/app";
import "firebase/firestore";

export const getAllData = async () => {
  const db = firebase.firestore();
  const result = [];
  await db
    .collection("Product")
    .get()
    .then(function (querySnapshot) {
      querySnapshot.forEach((doc) => {
        const item = {
          category: doc.data().Category,
          desc: doc.data().Desc,
          nama: doc.data().Name,
          price: doc.data().Price,
          photoUrl: doc.data().Url,
        };
        result.push(item);
      });
    });
  return result;
};
