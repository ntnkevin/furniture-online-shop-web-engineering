import firebase from "firebase";
import "firebase/firestore";
import "firebase/auth";

export const addDataToDb = (data) => {
  const db = firebase.firestore();
  db.collection("account")
    .add({
      name: data.name,
      email: data.email,
      alamat: data.alamat,
      noTelepon: data.noTelepon,
    })
    .then(() => {
      console.log("Data added to DB");
    });
};

export const signUp = async (data) => {
  try {
    await firebase.auth().signInWithEmailAndPassword(data.email, data.password);
    // console.log("signed in!");
  } catch (error) {
    console.log("Sign In error : " + error);
  }
};

export const editUserData = (data) => {
  var user = firebase.auth().currentUser;
  user
    .updateProfile({
      displayName: data.name,
      phoneNumber: data.noTelepon,
    })
    .catch(function (error) {
      console.log("Error when edit UserData " + error);
    });
};

export const signIn = async (data) => {
  try {
    const result = await firebase
      .auth()
      .signInWithEmailAndPassword(data.email, data.password);
    // console.log("signed in!");
    return result;
    // return result;
  } catch (error) {
    // alert(error);
    throw error;
  }
};

export const signOut = () => {
  firebase
    .auth()
    .signOut()
    .then(function () {
      alert("LOGOUT");
    })
    .catch(function (error) {
      console.log(error);
    });
};
