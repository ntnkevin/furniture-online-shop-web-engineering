import React from "react";
import withRoot from "../Moduls/WithRoot";
// import MainContex from "../MainContex";

import ProductHeader from "../Moduls/ProductHeader";
import ProductCategory from "../Moduls/ProductCategory";
import ProductHowItWork from "../Moduls/ProductHowItWork";

function Landing() {
  // const contex = React.useContext(MainContex);
  return (
    <div style={{marginTop:'63px'}}>
      <ProductHeader />
      <ProductCategory />
      <ProductHowItWork />
    </div>
  );
}

export default withRoot(Landing);
