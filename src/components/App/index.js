import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { MainProvider } from "../MainContex";
import * as ROUTES from "../../constants/routes";
import firebaseConfig from "../Firebase";
import firebase from "firebase/app";

//IMPORT PAGES
import Contact from "../Contact";
import Navigation from "../Navigation";
import LandingPage from "../Landing";
import SignUpPage from "../SignUp";
import SignInPage from "../SignIn";
import HomePage from "../Home";
import AccountPage from "../Account";
import AdminPage from "../Admin";
import Footer from "../Footer";
import About from "../About";
import OrderPage from "../Order";
import ProductPage from "../Product";
import TestPage from "../Test";
import DetailProduct from "../Product/DetailProduct";
import AdminEditProduct from "../Admin/EditProduct";
import PaymentPage from "../Payment";
import ListOrderPage from "../ListOrder";

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => (
  <div>
    <Router>
      <MainProvider>
        <Navigation />
        <Switch>
          <Route exact path={ROUTES.ADMIN} component={AdminPage} />
          <Route exact path={ROUTES.ACCOUNT} component={AccountPage} />
          <Route exact path={ROUTES.ABOUT} component={About} />
          <Route exact path={ROUTES.CONTACT} component={Contact} />
          <Route exact path={ROUTES.LANDING} component={LandingPage} />
          <Route exact path={ROUTES.SIGN_UP} component={SignUpPage} />
          <Route exact path={ROUTES.SIGN_IN} component={SignInPage} />
          <Route exact path={ROUTES.HOME} component={HomePage} />
          <Route exact path={ROUTES.TEST} component={TestPage} />
          <Route exact path={ROUTES.PRODUCT} component={ProductPage} />
          <Route exact path={ROUTES.LIST_ORDER} component={ListOrderPage} />
          <Route path="/payment/:id" component={PaymentPage} />
          <Route path="/product/detail/:id" component={DetailProduct} />
          <Route path="/product/buy/:id" component={OrderPage} />
          <Route path="/admin/edit-product/:id" component={AdminEditProduct} />
        </Switch>
        <Footer />
      </MainProvider>
    </Router>
  </div>
);

export default App;
