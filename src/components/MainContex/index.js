import React from "react";
import firebase from "firebase/app";
import "firebase/auth";

const MainContext = React.createContext({});

class MainProvider extends React.Component {
  state = {
    userName: "",
    email: "",
    role: "",
    setUserName: (userName) => this.setNewUserName(userName),
    setEmail: (email) => this.setNewEmail(email),
    setRole: (role) => this.setNewRole(role),
  };

  setNewUserName = (userName) => {
    this.setState({ userName: userName });
  };

  setNewEmail = (email) => {
    this.setState({ email: email });
  };

  setNewRole = (role) => {
    this.setState({ role: role });
  };

  checkAuth() {
    const setEmail = (email) => {
      this.setNewEmail(email);
    };

    const setName = (name) => {
      this.setNewUserName(name);
    };

    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in.
        setEmail(user.email);
        setName(user.setName);
        var userEmail = user.email;
        var userName = user.displayName;
        console.log("CONTEX Email : " + userEmail, "Name : " + userName);
      } else {
        console.log("Status : Empty User");
      }
    });
  }

  componentDidMount() {
    this.checkAuth();
  }

  render() {
    const { children } = this.props;
    // const { user, setUser } = this.state;
    return (
      <MainContext.Provider value={this.state}>{children}</MainContext.Provider>
    );
  }
}

export default MainContext;
export { MainProvider };
