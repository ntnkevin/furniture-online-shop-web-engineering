import React from "react";
import { makeStyles } from "@material-ui/core";
// import { Autorenew } from '@material-ui/icons';

const useStyle = makeStyles((theme) => ({
  headers: {
    fontFamily: "sans-serif",
    fontSize: "24px",
    fontWeight: "bold",
    textAlign: "center",
  },
  paragraph: {
    fontFamily: "sans-serif",
    fontSize: "30px",
    textAlign: "center",
    margin: "20px",
  },
  divs: {
    margin: "0 auto",
    padding: "20px 20px",
    boxShadow: "1px 1px 6px rgba(0,0,0,0.2)",
    width: "50%",
  },
  bigdiv: {
    margin: "10px auto",
    width: "90%",
    display: "grid",
    gridTemplateColumns: "auto auto auto auto",
    height: "775px",
    gridTemplateRows: "350px 350px",
  },
  divs2: {
    textAlign: "justify",
    padding: "10px 15px",
    boxShadow: "1px 1px 6px rgba(0,0,0,0.2)",
    margin: "12.5px 12.5px",
  },
}));

const AboutUS = () => {
  const classes = useStyle();
  return (
    <div>
      <h1>About Us</h1>
      <p style={{ fontSize: "40px", fontWeight: "bold", textAlign: "center" }}>
        We believe in the power of face to face connections.
      </p>
      <div className={classes.divs}>
        <h2 className={classes.headers}>Uncover Opportunities</h2>
        <p className={classes.paragraph}>
          Our mission is to unlock the business value of human connection.
        </p>
      </div>

      <br></br>
      <div className={classes.divs}>
        <h2 className={classes.headers}>Vision led, Customer driven.</h2>
        <p className={classes.paragraph}>
          To empower every organization to organize, connect and support unique
          communities.
        </p>
      </div>

      <br></br>
      <div className={classes.divs}>
        <h2 className={classes.headers}>Our Values</h2>
        <p className={classes.paragraph}>
          Values are the source of the impact we want to make in this world.
        </p>
      </div>
      <br></br>
      <div className={classes.bigdiv}>
        <div className={classes.divs2}>
          <h2 className={classes.headers} style={{ color: "#1f50af" }}>
            Be Fearless
          </h2>
          <p className={classes.paragraph}>
            We don't fear failure. In fact, we embrace it. We believe our
            failures lead to progress.
          </p>
        </div>
        <br></br>

        <div className={classes.divs2}>
          <h2 className={classes.headers} style={{ color: "#00a383" }}>
            Be Passionate
          </h2>
          <p className={classes.paragraph}>
            We are a passionate group of individuals that come together for one
            collective goal - to change the way people connect, learn, and grow.
          </p>
        </div>
        <br></br>

        <div className={classes.divs2}>
          <h2 className={classes.headers} style={{ color: "#3e34bc" }}>
            Be Remarkable
          </h2>
          <p className={classes.paragraph}>
            We are a diverse group of talented people who love to explore their
            talents inside and outside of the office.
          </p>
        </div>
        <br></br>
        <div className={classes.divs2}>
          <h2 className={classes.headers} style={{ color: "#b93636" }}>
            Be Transparent
          </h2>
          <p className={classes.paragraph}>
            We are honest with each other. We are not afraid to be honest
            because we know we are trying to better each other.
          </p>
        </div>
        <br></br>
      </div>
      <div style={{ clear: "left" }}></div>
    </div>
  );
};

export default AboutUS;
