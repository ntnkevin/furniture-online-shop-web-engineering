import React from "react";
import { Container } from "@material-ui/core";
// import { withAuthorization } from "../Session";

const HomePage = () => (
  <div>
    <Container maxwidth="sm" style={{marginTop:"100px" }}>
      <h1>Home Page</h1>
      <p>The Home Page is accessible by every signed in user.</p>
    </Container>
  </div>
);

// const condition = (authUser) => !!authUser;

export default HomePage;
