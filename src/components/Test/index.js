import React from "react";
import firebase from "firebase";
import "firebase/firestore";
// import { useParams } from "react-router-dom";

var orderProduct = [];

const OrderPage = () => {
  const [txtProductId, setTxtProductId] = React.useState("");
  const [txtQty, setTxtQty] = React.useState("");
  const [txtPrice, setTxtPrice] = React.useState("");
  // const [productList, setProductList] = React.useState([]);
  // const contex = React.useContext(MainContex);

  // const fetchProduct = async () => {
  //   const db = firebase.firestore();
  //   const result = [];
  //   const productRef = db.collection("Product").doc(id);
  //   await productRef.get().then((doc) => {
  //     const item = {
  //       category: doc.data().Category,
  //       desc: doc.data().Desc,
  //       nama: doc.data().Name,
  //       price: doc.data().Price,
  //       photoUrl: doc.data().Url,
  //     };
  //     result.push(item);
  //   });
  //   setProductList(result);
  // };

  // React.useEffect(() => {
  //   fetchProduct();
  // }, []);

  const addProduct = () => {
    let more = {
      idProduct: txtProductId,
      qty: txtQty,
      price: txtPrice,
    };

    orderProduct.push(more);
    console.log("MOREEE", orderProduct);
  };

  const handlerAddProduct = () => {
    try {
      addProduct();
      console.log(orderProduct);
    } catch (error) {
      console.log(error);
    }
  };

  const addOrderToDb = () => {
    const db = firebase.firestore();
    db.collection("order")
      .add({
        owner: "Greg",
        item: orderProduct,
      })
      .then(() => {
        console.log("Data added to DB");
      });
  };

  const handlerSubmit = () => {
    try {
      addProduct();
      addOrderToDb();
      console.log(orderProduct);
      alert("Order Created");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <h1> Hello</h1>
      <label>PRODUCT NAME : </label>
      <input
        type="text"
        value={txtProductId}
        onChange={(ev) => setTxtProductId(ev.target.value)}
      ></input>
      <br></br>
      <label>QTY : </label>
      <input
        type="text"
        value={txtQty}
        onChange={(ev) => setTxtQty(ev.target.value)}
      ></input>
      <br></br>
      <label>PRICE : </label>
      <input
        type="text"
        value={txtPrice}
        onChange={(ev) => setTxtPrice(ev.target.value)}
      ></input>
      {/* <br></br> */}
      <button onClick={handlerAddProduct}>Click To add more Product! !</button>
      <br></br>
      {/* {orderProduct} */}
      <button onClick={handlerSubmit}>Click To add Order! !</button>
    </div>
  );
};

export default OrderPage;
