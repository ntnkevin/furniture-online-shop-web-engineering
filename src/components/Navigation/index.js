import React from "react";
import clsx from "clsx";
import MainContex from "../MainContex";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { signOut } from "../controller/authController";
import MenuIcon from "@material-ui/icons/Menu";
import {
  ChevronLeft,
  ChevronRight,
  Person,
  AllInbox,
  ExitToApp,
  AccountBox,
  SupervisedUserCircle,
  ContactSupport,
  Info,
  AccountBalanceWallet,
} from "@material-ui/icons";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import { useHistory } from "react-router-dom";
import * as ROUTES from "../../constants/routes";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    backgroundColor: "black",
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    backgroundColor: "rgb(24,24,27)",
    color: "white",
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

const AppBarNonLogin = () => {
  const classes = useStyles();
  const history = useHistory();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon style={{ color: "white" }} />
          </IconButton>
          <Typography
            style={{ color: "#fff", fontWeight: "bold" }}
            variant="h6"
            noWrap
          >
            Shop Max
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Typography
          variant="h6"
          noWrap
          style={{ textAlign: "center", paddingTop: "50px", color: "#fff" }}
        >
          Menu Bar
        </Typography>
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeft style={{ color: "#fff" }} />
            ) : (
              <ChevronRight style={{ color: "#fff" }} />
            )}
          </IconButton>
        </div>
        <hr style={{ border: "1px solid white", width: "100%" }}></hr>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.LANDING);
            }}
          >
            <ListItemIcon>
              <InboxIcon style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Home"} />
          </ListItem>
        </List>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.PRODUCT);
            }}
          >
            <ListItemIcon>
              <AllInbox style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Product"} />
          </ListItem>
        </List>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.CONTACT);
            }}
          >
            <ListItemIcon>
              <ContactSupport style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Contact us"} />
          </ListItem>
        </List>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.ABOUT);
            }}
          >
            <ListItemIcon>
              <Info style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"About"} />
          </ListItem>
        </List>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.SIGN_IN);
            }}
          >
            <ListItemIcon>
              <Person style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Sign In"} />
          </ListItem>
        </List>
      </Drawer>
      <main></main>
    </div>
  );
};

const AppBarLogin = () => {
  const classes = useStyles();
  const history = useHistory();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const contex = React.useContext(MainContex);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const signOutHandler = () => {
    signOut();
    contex.setEmail("");
    history.push(ROUTES.LANDING);
  };

  const AdminList = () => {
    return (
      <div className={classes.root}>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.ADMIN);
            }}
          >
            <ListItemIcon>
              <SupervisedUserCircle style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Admin"} />
          </ListItem>
        </List>
      </div>
    );
  };

  const UserList = () => {
    return (
      <div className={classes.root}>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.ACCOUNT);
            }}
          >
            <ListItemIcon>
              <AccountBox style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Profile"} />
          </ListItem>
        </List>
      </div>
    );
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon style={{ color: "white" }} />
          </IconButton>
          <Typography
            style={{ color: "#fff", fontWeight: "bold" }}
            variant="h6"
            noWrap
          >
            Shop Max
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Typography
          variant="h6"
          noWrap
          style={{ textAlign: "center", paddingTop: "50px", color: "#fff" }}
        >
          Menu Bar
        </Typography>
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeft style={{ color: "#fff" }} />
            ) : (
              <ChevronRight style={{ color: "#fff" }} />
            )}
          </IconButton>
        </div>
        <hr style={{ border: "1px solid white", width: "100%" }}></hr>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.LANDING);
            }}
          >
            <ListItemIcon>
              <InboxIcon style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Home"} />
          </ListItem>
        </List>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.PRODUCT);
            }}
          >
            <ListItemIcon>
              <AllInbox style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Product"} />
          </ListItem>
        </List>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.CONTACT);
            }}
          >
            <ListItemIcon>
              <ContactSupport style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Contact us"} />
          </ListItem>
        </List>
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.ABOUT);
            }}
          >
            <ListItemIcon>
              <Info style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"About"} />
          </ListItem>
        </List>
        <hr style={{ border: "1px solid white", width: "100%" }}></hr>
        {contex.email === "admin@gmail.com" ? <AdminList /> : <UserList />}
        <List>
          <ListItem
            button
            onClick={() => {
              history.push(ROUTES.LIST_ORDER);
            }}
          >
            <ListItemIcon>
              <AccountBalanceWallet style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"List Order"} />
          </ListItem>
        </List>
        <List>
          <ListItem
            button
            onClick={() => {
              signOutHandler();
            }}
          >
            <ListItemIcon>
              <ExitToApp style={{ color: "#fff" }} />
            </ListItemIcon>
            <ListItemText primary={"Sign Out"} />
          </ListItem>
        </List>
      </Drawer>

      <main></main>
    </div>
  );
};

const Navbar = () => {
  const contex = React.useContext(MainContex);
  return (
    <div>{contex.email !== "" ? <AppBarLogin /> : <AppBarNonLogin />}</div>
  );
};
export default Navbar;
