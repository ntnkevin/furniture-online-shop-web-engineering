import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles((theme) => ({
  divs: {
    float: "left",
    textAlign: "center",
    boxShadow: "1px 1px 6px rgba(0,0,0,0.2)",
    height: "300px",
    width: "45%",
    padding: "0px 5px",
  },
}));

const ContactUS = () => {
  const classes = useStyle();
  return (
    <div>
      <div
        style={{ width: "80%", margin: "50px auto 50px auto", padding: "2%" }}
      >
        <center>
          <h1>Who do you need to get in touch with? </h1>
        </center>
        <div className={classes.divs} style={{ marginLeft: "2.5%" }}>
          <h2>Sales</h2>
          <p style={{ padding: "10px" }}>
            Interested in learning more about Shop Max? <br></br>Contact our
            product experts at
            <p style={{ color: "#fa591d" }}> sales@shopmax.com </p> or{" "}
            <div style={{ color: "#fa591d" }}>(415) 231-6360.</div>
          </p>
        </div>
        <div className={classes.divs} style={{ marginLeft: "5%" }}>
          <h2>Business Development</h2>
          <p style={{ padding: "10px" }}>
            Are you a reseller, affiliate, or association that would like to
            partner with Shop Max? Get connected with our Partner team at
            <p style={{ color: "#fa591d" }}> sales@shopmax.com </p>
          </p>
        </div>
        <div
          className={classes.divs}
          style={{ marginTop: "20px", marginLeft: "2.5%" }}
        >
          <h2>Support</h2>
          <p style={{ padding: "10px" }}>
            We have a support portal that has the answers to dozens of the most
            common questions about Shop Max! Check it out here.
            <br></br>
            If you don’t find what you need, please submit a support request
            through our help center or chat us through the Shop Max app.
          </p>
        </div>
        <div
          className={classes.divs}
          style={{ marginTop: "20px", marginLeft: "5%" }}
        >
          <h2>General Questions</h2>
          <p style={{ padding: "10px" }}>
            Have a general question for us? Contact us at{" "}
            <p style={{ color: "#fa591d" }}>sales@shopmax.com </p>
          </p>
        </div>
        <div style={{ clear: "left" }}></div>
      </div>
    </div>
  );
};

export default ContactUS;
