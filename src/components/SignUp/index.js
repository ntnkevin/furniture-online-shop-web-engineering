import React from "react";
import firebase from "firebase";
import "firebase/firestore";
import "firebase/auth";
import * as ROUTES from "../../constants/routes";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import { Container } from "@material-ui/core";
import { signUp } from "../controller/authController";

const SignUpPage = () => {
  const [txtEmail, setTxtEmail] = React.useState("");
  const [txtName, setTxtName] = React.useState("");
  const [txtPassword, setTxtPassword] = React.useState("");
  const [txtAddress, setTxtAddress] = React.useState("");
  const [txtPhoneNumber, setTxtPhoneNumber] = React.useState("");

  const createUser = async () => {
    const dataUser = {
      name: txtName,
      password: txtPassword,
      email: txtEmail,
      alamat: txtAddress,
      noTelepon: txtPhoneNumber,
    };

    try {
      addDataToDb(dataUser);
      await firebase
        .auth()
        .createUserWithEmailAndPassword(txtEmail, txtPassword);
      console.log("Register to auth done");
      alert("Register Done");
      // IF CREATE USER SUCCESS, THEN EXECUTE FUNCTION BELOW
      signUp(dataUser);
      editUserData(dataUser);
    } catch (error) {
      console.log(error);
      alert(error);
    }
  };

  const useStyle = makeStyles((theme) => ({
    root: {
      marginTop: "60px",
      marginLeft: "auto",
      marginRight: "auto",
      marginBottom: "25px",
      paddingTop: "10px",
      justifyContent: "center",
      paddingBottom: "10px",
      backgroundColor: "#f2f2f2",
      height: "300px",
    },
    buttons: {
      justifyContent: "center",
      backgroundColor: "rgba(27,27,27,0.8)",
      height: "30px",
      width: "100px",
      borderRadius: "10px",
      border: "1px solid #ebebeb",
      color: "white",
    },
    inputs: {
      borderRadius: "10px",
      padding: "0px 15px 0px 15px",
      background: "#fff",
      fontFamily: "arial",
      height: "25px",
      border: "1px solid #ebebeb",
    },
    divsignup: {
      paddingTop: "10px",
      margin: "25px auto",
      width: "500px",
      backgroundColor: "#fff",
      boxShadow: "1px 1px 6px rgba(0,0,0,0.2)",
      paddingBottom: "10px",
    },
    abu: {
      marginTop: "85px",
      paddingTop: "50px",
      marginBottom: "60px",
      paddingBottom: "1px",
    },
  }));

  const addDataToDb = () => {
    const db = firebase.firestore();
    db.collection("account")
      .add({
        name: txtName,
        email: txtEmail,
        alamat: txtAddress,
        noTelepon: txtPhoneNumber,
      })
      .then(() => {
        console.log("Data added to DB");
      });
  };

  const editUserData = () => {
    var user = firebase.auth().currentUser;
    user
      .updateProfile({
        displayName: txtName,
        phoneNumber: txtPhoneNumber,
      })
      .catch(function (error) {
        console.log("Error when edit UserData " + error);
      });
  };

  const handleSubmit = async () => {
    try {
      createUser();
      history.push(ROUTES.LANDING);
    } catch (error) {
      console.log("hadleSubmit Error : " + error);
    }
  };

  const classes = useStyle();
  const history = useHistory();

  const goBack = () => {
    history.goBack();
  };

  return (
    <div className={classes.root}>
      <center>
        <Container fix>
          <div className={classes.divsignup}>
            <h1>Create Account</h1>
            <table style={{ marginBottom: "10px" }}>
              <tr>
                <td>
                  <label>UserName</label>
                </td>
                <td>:</td>
                <td>
                  <input
                    type="text"
                    value={txtName}
                    className={classes.inputs}
                    onChange={(ev) => setTxtName(ev.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>
                  <label>Email</label>
                </td>
                <td>:</td>
                <td>
                  <input
                    type="text"
                    value={txtEmail}
                    className={classes.inputs}
                    onChange={(ev) => setTxtEmail(ev.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>
                  <label>Address</label>
                </td>
                <td>:</td>
                <td>
                  <input
                    type="text"
                    value={txtAddress}
                    className={classes.inputs}
                    onChange={(ev) => setTxtAddress(ev.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>
                  <label>Phone Number </label>
                </td>
                <td>:</td>
                <td>
                  <input
                    type="text"
                    value={txtPhoneNumber}
                    className={classes.inputs}
                    onChange={(ev) => setTxtPhoneNumber(ev.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>
                  <label>Password</label>
                </td>
                <td>:</td>
                <td>
                  <input
                    type="password"
                    value={txtPassword}
                    className={classes.inputs}
                    onChange={(ev) => setTxtPassword(ev.target.value)}
                  />
                </td>
              </tr>
            </table>
            <button className={classes.buttons} onClick={handleSubmit}>
              Register
            </button>
            &nbsp;
            <button className={classes.buttons} onClick={goBack}>
              Cancel
            </button>
          </div>
        </Container>
      </center>
    </div>
  );
};
export default SignUpPage;

// const INITIAL_STATE = {
//   username: "",
//   email: "",
//   passwordOne: "",
//   passwordTwo: "",
//   error: null,
// };

// const SignUpForm = withRouter(withFirebase(SignUpFormBase));
// export { SignUpForm, SignUpLink };

// class SignUpFormBase extends Component {
//   constructor(props) {
//     super(props);
//     this.state = { ...INITIAL_STATE };
//   }

//   onSubmit = (event) => {
//     const { username, email, passwordOne } = this.state;
//     const db = firebase.firestore();
//     db.collection("Account")
//       .add({
//         name: username,
//         email: email,
//       })
//       .then(() => {
//         alert("Data Inputed!");
//       });

//     this.props.firebase
//       .doCreateUserWithEmailAndPassword(email, passwordOne)
//       .then((authUser) => {
//         // Create a user in your Firebase realtime database
//         this.props.firebase
//           .user(authUser.user.uid)
//           .set({
//             username,
//             email,
//           })
//           .then(() => {
//             this.setState({ ...INITIAL_STATE });
//             this.props.history.push(ROUTES.HOME);
//           })
//           .catch((error) => {
//             this.setState({ error });
//           });
//       })
//       .catch((error) => {
//         this.setState({ error });
//       });

//     event.preventDefault();
//   };

//   onChange = (event) => {
//     this.setState({ [event.target.name]: event.target.value });
//   };

//   render() {
//     const { username, email, passwordOne, passwordTwo, error } = this.state;

//     const isInvalid =
//       passwordOne !== passwordTwo ||
//       passwordOne === "" ||
//       email === "" ||
//       username === "";

//     return (
//       <form onSubmit={this.onSubmit}>
//         <tr>
//           <td style={{ textAlign: "left" }}>Name;</td>
//           <td>: &nbsp;</td>
//           <td>
//             <input
//               name="username"
//               value={username}
//               onChange={this.onChange}
//               type="text"
//               placeholder="Full Name"
//             />
//           </td>
//         </tr>
//         <tr>
//           <td style={{ textAlign: "left" }}>E-mail</td>
//           <td>: &nbsp;</td>
//           <td>
//             <input
//               name="email"
//               value={email}
//               onChange={this.onChange}
//               type="text"
//               placeholder="Email Address"
//             />
//           </td>
//         </tr>
//         <tr>
//           <td style={{ textAlign: "left" }}>Password &nbsp;</td>
//           <td>: &nbsp;</td>
//           <td>
//             <input
//               name="passwordOne"
//               value={passwordOne}
//               onChange={this.onChange}
//               type="password"
//               placeholder="Password"
//             />
//           </td>
//         </tr>
//         <tr>
//           <td style={{ textAlign: "left" }}>Confirm Password </td>
//           <td>: &nbsp;</td>
//           <td>
//             <input
//               name="passwordTwo"
//               value={passwordTwo}
//               onChange={this.onChange}
//               type="password"
//               placeholder="Confirm Password"
//             />
//           </td>
//         </tr>
//         <br></br>
//         <Button
//           variant="contained"
//           color="primary"
//           disabled={isInvalid}
//           type="submit"
//         >
//           Sign Up
//         </Button>

//         {error && <p>{error.message}</p>}
//       </form>
//     );
//   }
// }
