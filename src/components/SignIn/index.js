import React from "react";
import { Container } from "@material-ui/core";
// import firebase from "firebase/app";
import "firebase/auth";
import { Link } from "react-router-dom";
import * as ROUTES from "../../constants/routes";
import { useHistory } from "react-router-dom";
import MainContex from "../MainContex";
import { makeStyles } from "@material-ui/core";
import { signIn } from "../controller/authController";

const useStyle = makeStyles((theme) => ({
  buttons: {
    justifyContent: "center",
    backgroundColor: 'rgba(27,27,27,0.8)',
    height: "30px",
    width: "100px",
    borderRadius: "10px",
    border: "1px solid #ebebeb",
    color: "white",
  },
  inputs: {
      borderRadius: '10px',
      padding: '0px 15px 0px 15px',
      background: '#fff',
      fontFamily: 'arial',
      height: '25px',
      border: '1px solid #ebebeb',
      marginBottom:'5px'
  },
  divlogin: {
    paddingTop: "15px",
    margin: "0px auto",
    width: "500px",
    backgroundColor: "#fff",
    boxShadow: "1px 1px 6px rgba(0,0,0,0.2)",
  },
  abu: {
    backgroundColor: "#f2f2f2",
    marginTop: "60px",
    paddingTop: "35px",
  },
}));

const SignUpTxt = () => {
  return (
    <div>
      <Container fiexd>
        <h2> Not have account?</h2>
        <p style={{ marginBottom: "0px", paddingBottom: "15px" }}>
          Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
        </p>
      </Container>
    </div>
  );
};

const SignInPage = () => {
  const [txtEmail, setEmail] = React.useState("");
  const [txtPassword, setPassword] = React.useState("");

  const dataUser = {
    email: txtEmail,
    password: txtPassword,
  };

  const history = useHistory();
  const classes = useStyle();
  const contex = React.useContext(MainContex);

  const handleSubmit = async () => {
    signIn(dataUser)
      .then(async (result) => {
        console.log(result);
        await contex.setEmail(txtEmail);
        alert("success");
        history.push(ROUTES.LANDING);
      })
      .catch((err) => {
        alert(err);
      });
  };

  return (
    <div className={classes.abu}>
      <center>
        <Container fixed>
          <div className={classes.divlogin}>
            <h1> Welcome!</h1>
            <table>
              <tr>
                <td>
                  <input
                    type="text"
                    className={classes.inputs}
                    placeholder="E-mail"
                    value={txtEmail}
                    onChange={(ev) => {
                      setEmail(ev.target.value);
                    }}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input
                    type="password"
                    className={classes.inputs}
                    placeholder="Password"
                    value={txtPassword}
                    onChange={(ev) => {
                      setPassword(ev.target.value);
                    }}
                  />
                </td>
              </tr>
            </table>
            <center style={{ paddingBottom: "15px" }}>
              <button className={classes.buttons} onClick={handleSubmit}>
                Login!
              </button>
            </center>
          </div>
        </Container>
        <SignUpTxt />
      </center>
    </div>
  );
};

export default SignInPage;
