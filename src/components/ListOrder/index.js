import React from "react";
import MainContex from "../MainContex";
import firebase from "firebase/app";
import "firebase/firestore";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";

const useStyle = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    paddingBottom: "15px",
    paddingTop: "20px",
  },
  header: {
    textAlign: "center",
  },
  divbig: {
    margin: "0px auto",
    marginTop: "60px",
    marginBottom: "0px",
    paddingTop: "50px",
    width: "1000px",
    backgroundColor: "#fff",
  },
  content: {},
  row: {
    float: "left",
    width: "50%",
    padding: "10px",
  },
  column: {
    content: "",
    display: "table",
    clear: "both",
  },
}));

const ListOrder = () => {
  const classes = useStyle();
  const history = useHistory();
  const contex = React.useContext(MainContex);
  const [menuList, setMenuList] = React.useState([]);

  const fetchMenu = async () => {
    const db = firebase.firestore();
    const result = [];
    await db
      .collection("order")
      .where("owner", "==", contex.email)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach((doc) => {
          const item = {
            docId: doc.id,
            status: doc.data().status,
          };
          result.push(item);
        });
      });
    setMenuList(result);
  };

  React.useEffect(() => {
    fetchMenu();
    //eslint-disable-next-line
  }, []);

  const PaidButton = () => {
    return (
      <Button style={{ backgroundColor: "rgba(27,27,27,0.8)", color: "white" }}>
        Already Paid
      </Button>
    );
  };

  const UnpaidButton = (props) => {
    return (
      <Button
        style={{ backgroundColor: "rgba(27,27,27,0.8)", color: "white" }}
        onClick={() => {
          history.push(`/payment/${props.docId}`);
        }}
      >
        Pay here
      </Button>
    );
  };

  const Order = () => {
    return (
      <div>
        <center>
          <table>
            <tr>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>Document id</h1>
              </th>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>Status</h1>
              </th>
              <th style={{ paddingRight: "15px", paddingLeft: "15px" }}>
                <h1>Pay Here</h1>
              </th>
            </tr>
            {menuList.map((item, key) => (
              <tr key={key}>
                <td
                  style={{
                    borderRight: "1px solid black",
                    marginRight: "30px",
                  }}
                >
                  <h3>{item.docId}</h3>
                </td>
                <td
                  style={{
                    borderRight: "1px solid black",
                    paddingRight: "30px",
                    paddingLeft: "30px",
                  }}
                >
                  <h3>{item.status}</h3>
                </td>
                <td style={{ paddingRight: "30px", paddingLeft: "30px" }}>
                  {item.status === "PAID" ? (
                    <PaidButton />
                  ) : (
                    <UnpaidButton docId={item.docId} />
                  )}
                </td>
              </tr>
            ))}
          </table>
        </center>
      </div>
    );
  };

  return (
    <div className={classes.root}>
      <div className={classes.divbig}>
        <h1>Hello {contex.email}</h1>
        <h1>Here is your list order : </h1>
        <Order />
      </div>
    </div>
  );
};

export default ListOrder;
